EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-regulators
LIBS:my-relays
LIBS:my-power-supplies
LIBS:step-up-bistable-relay-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R2
U 1 1 60ACE149
P 5300 4500
F 0 "R2" V 5380 4500 50  0000 C CNN
F 1 "390" V 5300 4500 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5230 4500 50  0001 C CNN
F 3 "" H 5300 4500 50  0000 C CNN
	1    5300 4500
	0    1    1    0   
$EndComp
$Comp
L R R1
U 1 1 60ACE150
P 5300 4100
F 0 "R1" V 5380 4100 50  0000 C CNN
F 1 "390" V 5300 4100 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5230 4100 50  0001 C CNN
F 3 "" H 5300 4100 50  0000 C CNN
	1    5300 4100
	0    1    1    0   
$EndComp
$Comp
L D D2
U 1 1 60ACE157
P 6050 3450
F 0 "D2" H 6050 3550 50  0000 C CNN
F 1 "1N4148" H 6050 3350 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 6050 3450 50  0001 C CNN
F 3 "" H 6050 3450 50  0000 C CNN
	1    6050 3450
	0    1    1    0   
$EndComp
$Comp
L D D1
U 1 1 60ACE15E
P 5650 3450
F 0 "D1" H 5650 3550 50  0000 C CNN
F 1 "1N4148" H 5650 3350 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 5650 3450 50  0001 C CNN
F 3 "" H 5650 3450 50  0000 C CNN
	1    5650 3450
	0    1    1    0   
$EndComp
$Comp
L PN2222A Q2
U 1 1 60ACE165
P 6150 4100
F 0 "Q2" H 6350 4175 50  0000 L CNN
F 1 "2N2222" H 6350 4100 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 6350 4025 50  0001 L CIN
F 3 "" H 6150 4100 50  0000 L CNN
	1    6150 4100
	1    0    0    -1  
$EndComp
$Comp
L PN2222A Q1
U 1 1 60ACE16C
P 5750 4500
F 0 "Q1" H 5950 4575 50  0000 L CNN
F 1 "2N2222" H 5950 4500 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 5950 4425 50  0001 L CIN
F 3 "" H 5750 4500 50  0000 L CNN
	1    5750 4500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 60ACE173
P 5850 4900
F 0 "#PWR01" H 5850 4650 50  0001 C CNN
F 1 "GND" H 5850 4750 50  0000 C CNN
F 2 "" H 5850 4900 50  0000 C CNN
F 3 "" H 5850 4900 50  0000 C CNN
	1    5850 4900
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR02
U 1 1 60ACE179
P 3700 2550
F 0 "#PWR02" H 3700 2400 50  0001 C CNN
F 1 "+5V" H 3700 2690 50  0000 C CNN
F 2 "" H 3700 2550 50  0000 C CNN
F 3 "" H 3700 2550 50  0000 C CNN
	1    3700 2550
	1    0    0    -1  
$EndComp
$Comp
L OMRON-G5RL-K1A-E K1
U 1 1 60ACE17F
P 7250 2850
F 0 "K1" H 7200 3250 50  0000 C CNN
F 1 "OMRON-G5RL-K1A-E" H 7245 2050 50  0000 C CNN
F 2 "myRelays:Relay_SPST_Omron-G5RL-K1A-E" H 7250 2850 50  0001 C CNN
F 3 "" H 7250 2850 50  0000 C CNN
	1    7250 2850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 60ACE2B7
P 3650 3600
F 0 "#PWR03" H 3650 3350 50  0001 C CNN
F 1 "GND" H 3650 3450 50  0000 C CNN
F 2 "" H 3650 3600 50  0000 C CNN
F 3 "" H 3650 3600 50  0000 C CNN
	1    3650 3600
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P2
U 1 1 60ACE487
P 8150 2750
F 0 "P2" H 8150 2900 50  0000 C CNN
F 1 "OUT" V 8250 2750 50  0000 C CNN
F 2 "Connect:bornier2" H 8150 2750 50  0001 C CNN
F 3 "" H 8150 2750 50  0000 C CNN
	1    8150 2750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 60ACD46D
P 5300 3500
F 0 "#PWR04" H 5300 3250 50  0001 C CNN
F 1 "GND" H 5300 3350 50  0000 C CNN
F 2 "" H 5300 3500 50  0000 C CNN
F 3 "" H 5300 3500 50  0000 C CNN
	1    5300 3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 60ACD4D2
P 4000 3500
F 0 "#PWR05" H 4000 3250 50  0001 C CNN
F 1 "GND" H 4000 3350 50  0000 C CNN
F 2 "" H 4000 3500 50  0000 C CNN
F 3 "" H 4000 3500 50  0000 C CNN
	1    4000 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3200 4100 3200
Wire Wire Line
	4000 3400 4100 3400
Wire Wire Line
	4000 3500 4000 3400
Wire Wire Line
	5300 3400 5200 3400
Wire Wire Line
	5300 3500 5300 3400
Wire Wire Line
	5200 3200 6850 3200
Connection ~ 6600 2600
Wire Wire Line
	7650 2700 7950 2700
Wire Wire Line
	3650 3500 3650 3600
Wire Wire Line
	3450 3500 3650 3500
Wire Wire Line
	3450 3400 3800 3400
Wire Wire Line
	3800 3400 3800 4500
Wire Wire Line
	3850 3100 3850 4100
Wire Wire Line
	3450 3300 3850 3300
Connection ~ 5850 3800
Wire Wire Line
	5650 3800 5850 3800
Wire Wire Line
	5650 3600 5650 3800
Wire Wire Line
	6050 3200 6050 3300
Connection ~ 6250 3800
Wire Wire Line
	6050 3800 6250 3800
Wire Wire Line
	6050 3600 6050 3800
Wire Wire Line
	6250 3500 6250 3900
Wire Wire Line
	6250 4800 6250 4300
Wire Wire Line
	5450 4100 5950 4100
Wire Wire Line
	5850 3100 5850 4300
Wire Wire Line
	5850 4700 5850 4900
Wire Wire Line
	5450 4500 5550 4500
Wire Wire Line
	3800 4500 5150 4500
Wire Wire Line
	5850 4800 6250 4800
Connection ~ 5850 4800
Wire Wire Line
	3850 4100 5150 4100
Connection ~ 5650 3200
Wire Wire Line
	6600 2600 6850 2600
Wire Wire Line
	6600 2900 6850 2900
Wire Wire Line
	6850 3500 6250 3500
Wire Wire Line
	5850 3100 6850 3100
Connection ~ 6050 3200
Wire Wire Line
	7750 3000 7650 3000
Connection ~ 7750 2700
Wire Wire Line
	7750 2700 7750 2700
Wire Wire Line
	7750 2700 7750 3000
Wire Wire Line
	7850 2300 6600 2300
Wire Wire Line
	6600 2300 6600 2900
Text GLabel 3700 3100 1    60   Input ~ 0
SET
Text GLabel 3850 3100 1    60   Input ~ 0
RESET
Wire Wire Line
	3700 3100 3700 3400
Connection ~ 3700 3400
Connection ~ 3850 3300
$Comp
L VCC #PWR06
U 1 1 60ACDF80
P 4000 3100
F 0 "#PWR06" H 4000 2950 50  0001 C CNN
F 1 "VCC" H 4000 3250 50  0000 C CNN
F 2 "" H 4000 3100 50  0000 C CNN
F 3 "" H 4000 3100 50  0000 C CNN
	1    4000 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3100 4000 3200
Connection ~ 4000 3200
$Comp
L CONN_01X05 P1
U 1 1 60AD1319
P 3250 3300
F 0 "P1" H 3250 3600 50  0000 C CNN
F 1 "IN" V 3350 3300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05" H 3250 3300 50  0001 C CNN
F 3 "" H 3250 3300 50  0000 C CNN
	1    3250 3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	3450 3100 3550 3100
Wire Wire Line
	3550 3100 3550 2650
Wire Wire Line
	3550 2650 5300 2650
Wire Wire Line
	5300 2650 5300 3200
Connection ~ 5300 3200
Wire Wire Line
	5650 3300 5650 3200
Wire Wire Line
	3700 2550 3700 2650
Connection ~ 3700 2650
Wire Wire Line
	7950 2800 7850 2800
Wire Wire Line
	7850 2800 7850 2300
$Comp
L XR2981-MODULE U1
U 1 1 60AF5E87
P 4650 3300
F 0 "U1" H 4650 3550 60  0000 C CNN
F 1 "XR2981-MODULE" H 4300 3050 60  0000 L CNN
F 2 "myPowerSupplies:XR2981-MODULE" H 4650 3300 60  0001 C CNN
F 3 "" H 4650 3300 60  0000 C CNN
	1    4650 3300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
