EESchema Schematic File Version 2
LIBS:mcp23008-eight-relay-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-regulators
LIBS:my-relays
LIBS:my-power-supplies
LIBS:my-switches
LIBS:my-ICs
LIBS:mcp23008-eight-relay-cache
EELAYER 25 0
EELAYER END
$Descr A3 11693 16535 portrait
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR01
U 1 1 60ACE2B7
P 2600 10100
F 0 "#PWR01" H 2600 9850 50  0001 C CNN
F 1 "GND" H 2600 9950 50  0000 C CNN
F 2 "" H 2600 10100 50  0000 C CNN
F 3 "" H 2600 10100 50  0000 C CNN
	1    2600 10100
	1    0    0    -1  
$EndComp
$Comp
L MCP23008-RESCUE-mcp23008-eight-relay U1
U 1 1 60AF73BA
P 3600 8700
F 0 "U1" H 3600 8850 50  0000 C CNN
F 1 "MCP23008" H 3650 8750 50  0000 C CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_LongPads" H 3600 8700 60  0001 C CNN
F 3 "" H 3600 8700 60  0000 C CNN
	1    3600 8700
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 60AF73C1
P 2200 8250
F 0 "#PWR02" H 2200 8000 50  0001 C CNN
F 1 "GND" H 2200 8100 50  0000 C CNN
F 2 "" H 2200 8250 50  0000 C CNN
F 3 "" H 2200 8250 50  0000 C CNN
	1    2200 8250
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 60AF8691
P 4900 7700
F 0 "R4" V 4980 7700 50  0000 C CNN
F 1 "680" V 4900 7700 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4830 7700 50  0001 C CNN
F 3 "" H 4900 7700 50  0000 C CNN
	1    4900 7700
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 60AF8697
P 4900 7300
F 0 "R3" V 4980 7300 50  0000 C CNN
F 1 "680" V 4900 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4830 7300 50  0001 C CNN
F 3 "" H 4900 7300 50  0000 C CNN
	1    4900 7300
	0    1    1    0   
$EndComp
$Comp
L D D6
U 1 1 60AF869D
P 5850 6650
F 0 "D6" H 5850 6750 50  0000 C CNN
F 1 "1N4148" H 5850 6550 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 5850 6650 50  0001 C CNN
F 3 "" H 5850 6650 50  0000 C CNN
	1    5850 6650
	0    1    1    0   
$EndComp
$Comp
L D D2
U 1 1 60AF86A3
P 5450 6650
F 0 "D2" H 5450 6750 50  0000 C CNN
F 1 "1N4148" H 5450 6550 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 5450 6650 50  0001 C CNN
F 3 "" H 5450 6650 50  0000 C CNN
	1    5450 6650
	0    1    1    0   
$EndComp
$Comp
L PN2222A Q6
U 1 1 60AF86A9
P 5750 7300
F 0 "Q6" H 5950 7375 50  0000 L CNN
F 1 "2N2222" H 5950 7300 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 5950 7225 50  0001 L CIN
F 3 "" H 5750 7300 50  0000 L CNN
	1    5750 7300
	1    0    0    -1  
$EndComp
$Comp
L PN2222A Q2
U 1 1 60AF86AF
P 5350 7700
F 0 "Q2" H 5550 7775 50  0000 L CNN
F 1 "2N2222" H 5550 7700 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 5550 7625 50  0001 L CIN
F 3 "" H 5350 7700 50  0000 L CNN
	1    5350 7700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 60AF86B5
P 5450 8100
F 0 "#PWR03" H 5450 7850 50  0001 C CNN
F 1 "GND" H 5450 7950 50  0000 C CNN
F 2 "" H 5450 8100 50  0000 C CNN
F 3 "" H 5450 8100 50  0000 C CNN
	1    5450 8100
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 60AF897C
P 4900 5400
F 0 "R2" V 4980 5400 50  0000 C CNN
F 1 "680" V 4900 5400 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4830 5400 50  0001 C CNN
F 3 "" H 4900 5400 50  0000 C CNN
	1    4900 5400
	0    1    1    0   
$EndComp
$Comp
L R R1
U 1 1 60AF8982
P 4900 5000
F 0 "R1" V 4980 5000 50  0000 C CNN
F 1 "680" V 4900 5000 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4830 5000 50  0001 C CNN
F 3 "" H 4900 5000 50  0000 C CNN
	1    4900 5000
	0    1    1    0   
$EndComp
$Comp
L D D5
U 1 1 60AF8988
P 5850 4350
F 0 "D5" H 5850 4450 50  0000 C CNN
F 1 "1N4148" H 5850 4250 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 5850 4350 50  0001 C CNN
F 3 "" H 5850 4350 50  0000 C CNN
	1    5850 4350
	0    1    1    0   
$EndComp
$Comp
L D D1
U 1 1 60AF898E
P 5450 4350
F 0 "D1" H 5450 4450 50  0000 C CNN
F 1 "1N4148" H 5450 4250 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 5450 4350 50  0001 C CNN
F 3 "" H 5450 4350 50  0000 C CNN
	1    5450 4350
	0    1    1    0   
$EndComp
$Comp
L PN2222A Q5
U 1 1 60AF8994
P 5750 5000
F 0 "Q5" H 5950 5075 50  0000 L CNN
F 1 "2N2222" H 5950 5000 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 5950 4925 50  0001 L CIN
F 3 "" H 5750 5000 50  0000 L CNN
	1    5750 5000
	1    0    0    -1  
$EndComp
$Comp
L PN2222A Q1
U 1 1 60AF899A
P 5350 5400
F 0 "Q1" H 5550 5475 50  0000 L CNN
F 1 "2N2222" H 5550 5400 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 5550 5325 50  0001 L CIN
F 3 "" H 5350 5400 50  0000 L CNN
	1    5350 5400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 60AF89A0
P 5450 5800
F 0 "#PWR04" H 5450 5550 50  0001 C CNN
F 1 "GND" H 5450 5650 50  0000 C CNN
F 2 "" H 5450 5800 50  0000 C CNN
F 3 "" H 5450 5800 50  0000 C CNN
	1    5450 5800
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR05
U 1 1 60AF98B4
P 5450 4000
F 0 "#PWR05" H 5450 3850 50  0001 C CNN
F 1 "VCC" H 5450 4150 50  0000 C CNN
F 2 "" H 5450 4000 50  0000 C CNN
F 3 "" H 5450 4000 50  0000 C CNN
	1    5450 4000
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR06
U 1 1 60AF992C
P 5450 6300
F 0 "#PWR06" H 5450 6150 50  0001 C CNN
F 1 "VCC" H 5450 6450 50  0000 C CNN
F 2 "" H 5450 6300 50  0000 C CNN
F 3 "" H 5450 6300 50  0000 C CNN
	1    5450 6300
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR07
U 1 1 60AF9B36
P 2600 9100
F 0 "#PWR07" H 2600 8950 50  0001 C CNN
F 1 "VCC" H 2600 9250 50  0000 C CNN
F 2 "" H 2600 9100 50  0000 C CNN
F 3 "" H 2600 9100 50  0000 C CNN
	1    2600 9100
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR08
U 1 1 60AFB3EC
P 2900 9150
F 0 "#PWR08" H 2900 9000 50  0001 C CNN
F 1 "VDD" H 2900 9300 50  0000 C CNN
F 2 "" H 2900 9150 50  0000 C CNN
F 3 "" H 2900 9150 50  0000 C CNN
	1    2900 9150
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 60C8E4D8
P 2900 9550
F 0 "R9" V 2980 9550 50  0000 C CNN
F 1 "4.7K" V 2900 9550 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 2830 9550 50  0001 C CNN
F 3 "" H 2900 9550 50  0000 C CNN
	1    2900 9550
	-1   0    0    1   
$EndComp
$Comp
L R R10
U 1 1 60C8E605
P 3100 9550
F 0 "R10" V 3180 9550 50  0000 C CNN
F 1 "4.7K" V 3100 9550 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 3030 9550 50  0001 C CNN
F 3 "" H 3100 9550 50  0000 C CNN
	1    3100 9550
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X03 P2
U 1 1 60D0BA87
P 7800 3900
F 0 "P2" H 7800 4100 50  0000 C CNN
F 1 "OUT1" V 7900 3900 50  0000 C CNN
F 2 "Connect:bornier3" H 7800 3900 50  0001 C CNN
F 3 "" H 7800 3900 50  0000 C CNN
	1    7800 3900
	1    0    0    1   
$EndComp
$Comp
L R R8
U 1 1 60D1039D
P 4900 12300
F 0 "R8" V 4980 12300 50  0000 C CNN
F 1 "680" V 4900 12300 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4830 12300 50  0001 C CNN
F 3 "" H 4900 12300 50  0000 C CNN
	1    4900 12300
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 60D103A3
P 4900 11900
F 0 "R7" V 4980 11900 50  0000 C CNN
F 1 "680" V 4900 11900 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4830 11900 50  0001 C CNN
F 3 "" H 4900 11900 50  0000 C CNN
	1    4900 11900
	0    1    1    0   
$EndComp
$Comp
L D D8
U 1 1 60D103A9
P 5850 11250
F 0 "D8" H 5850 11350 50  0000 C CNN
F 1 "1N4148" H 5850 11150 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 5850 11250 50  0001 C CNN
F 3 "" H 5850 11250 50  0000 C CNN
	1    5850 11250
	0    1    1    0   
$EndComp
$Comp
L D D4
U 1 1 60D103AF
P 5450 11250
F 0 "D4" H 5450 11350 50  0000 C CNN
F 1 "1N4148" H 5450 11150 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 5450 11250 50  0001 C CNN
F 3 "" H 5450 11250 50  0000 C CNN
	1    5450 11250
	0    1    1    0   
$EndComp
$Comp
L PN2222A Q8
U 1 1 60D103B5
P 5750 11900
F 0 "Q8" H 5950 11975 50  0000 L CNN
F 1 "2N2222" H 5950 11900 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 5950 11825 50  0001 L CIN
F 3 "" H 5750 11900 50  0000 L CNN
	1    5750 11900
	1    0    0    -1  
$EndComp
$Comp
L PN2222A Q4
U 1 1 60D103BB
P 5350 12300
F 0 "Q4" H 5550 12375 50  0000 L CNN
F 1 "2N2222" H 5550 12300 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 5550 12225 50  0001 L CIN
F 3 "" H 5350 12300 50  0000 L CNN
	1    5350 12300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 60D103C1
P 5450 12700
F 0 "#PWR09" H 5450 12450 50  0001 C CNN
F 1 "GND" H 5450 12550 50  0000 C CNN
F 2 "" H 5450 12700 50  0000 C CNN
F 3 "" H 5450 12700 50  0000 C CNN
	1    5450 12700
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 60D103D9
P 4900 10000
F 0 "R6" V 4980 10000 50  0000 C CNN
F 1 "680" V 4900 10000 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4830 10000 50  0001 C CNN
F 3 "" H 4900 10000 50  0000 C CNN
	1    4900 10000
	0    1    1    0   
$EndComp
$Comp
L R R5
U 1 1 60D103DF
P 4900 9600
F 0 "R5" V 4980 9600 50  0000 C CNN
F 1 "680" V 4900 9600 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4830 9600 50  0001 C CNN
F 3 "" H 4900 9600 50  0000 C CNN
	1    4900 9600
	0    1    1    0   
$EndComp
$Comp
L D D7
U 1 1 60D103E5
P 5850 8950
F 0 "D7" H 5850 9050 50  0000 C CNN
F 1 "1N4148" H 5850 8850 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 5850 8950 50  0001 C CNN
F 3 "" H 5850 8950 50  0000 C CNN
	1    5850 8950
	0    1    1    0   
$EndComp
$Comp
L D D3
U 1 1 60D103EB
P 5450 8950
F 0 "D3" H 5450 9050 50  0000 C CNN
F 1 "1N4148" H 5450 8850 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 5450 8950 50  0001 C CNN
F 3 "" H 5450 8950 50  0000 C CNN
	1    5450 8950
	0    1    1    0   
$EndComp
$Comp
L PN2222A Q7
U 1 1 60D103F1
P 5750 9600
F 0 "Q7" H 5950 9675 50  0000 L CNN
F 1 "2N2222" H 5950 9600 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 5950 9525 50  0001 L CIN
F 3 "" H 5750 9600 50  0000 L CNN
	1    5750 9600
	1    0    0    -1  
$EndComp
$Comp
L PN2222A Q3
U 1 1 60D103F7
P 5350 10000
F 0 "Q3" H 5550 10075 50  0000 L CNN
F 1 "2N2222" H 5550 10000 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 5550 9925 50  0001 L CIN
F 3 "" H 5350 10000 50  0000 L CNN
	1    5350 10000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 60D103FD
P 5450 10400
F 0 "#PWR010" H 5450 10150 50  0001 C CNN
F 1 "GND" H 5450 10250 50  0000 C CNN
F 2 "" H 5450 10400 50  0000 C CNN
F 3 "" H 5450 10400 50  0000 C CNN
	1    5450 10400
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR011
U 1 1 60D1041D
P 5450 8600
F 0 "#PWR011" H 5450 8450 50  0001 C CNN
F 1 "VCC" H 5450 8750 50  0000 C CNN
F 2 "" H 5450 8600 50  0000 C CNN
F 3 "" H 5450 8600 50  0000 C CNN
	1    5450 8600
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR012
U 1 1 60D10423
P 5450 10900
F 0 "#PWR012" H 5450 10750 50  0001 C CNN
F 1 "VCC" H 5450 11050 50  0000 C CNN
F 2 "" H 5450 10900 50  0000 C CNN
F 3 "" H 5450 10900 50  0000 C CNN
	1    5450 10900
	1    0    0    -1  
$EndComp
$Comp
L RELAY_SPDT K1
U 1 1 60D0B588
P 6850 3850
F 0 "K1" H 6800 3900 50  0000 C CNN
F 1 "FINDER-36.11.9.005" H 6850 3350 50  0000 C CNN
F 2 "myRelays:SRD-5VDC-SL-C" H 6850 3850 50  0001 C CNN
F 3 "" H 6850 3850 50  0000 C CNN
	1    6850 3850
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P3
U 1 1 60D0BE8E
P 7800 5050
F 0 "P3" H 7800 5250 50  0000 C CNN
F 1 "OUT1" V 7900 5050 50  0000 C CNN
F 2 "Connect:bornier3" H 7800 5050 50  0001 C CNN
F 3 "" H 7800 5050 50  0000 C CNN
	1    7800 5050
	1    0    0    1   
$EndComp
$Comp
L RELAY_SPDT K2
U 1 1 60D0BE9C
P 6850 5000
F 0 "K2" H 6800 5050 50  0000 C CNN
F 1 "FINDER-36.11.9.005" H 6850 4500 50  0000 C CNN
F 2 "myRelays:SRD-5VDC-SL-C" H 6850 5000 50  0001 C CNN
F 3 "" H 6850 5000 50  0000 C CNN
	1    6850 5000
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P4
U 1 1 60D0C112
P 7800 6200
F 0 "P4" H 7800 6400 50  0000 C CNN
F 1 "OUT1" V 7900 6200 50  0000 C CNN
F 2 "Connect:bornier3" H 7800 6200 50  0001 C CNN
F 3 "" H 7800 6200 50  0000 C CNN
	1    7800 6200
	1    0    0    1   
$EndComp
$Comp
L RELAY_SPDT K3
U 1 1 60D0C120
P 6850 6150
F 0 "K3" H 6800 6200 50  0000 C CNN
F 1 "FINDER-36.11.9.005" H 6850 5650 50  0000 C CNN
F 2 "myRelays:SRD-5VDC-SL-C" H 6850 6150 50  0001 C CNN
F 3 "" H 6850 6150 50  0000 C CNN
	1    6850 6150
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P5
U 1 1 60D0C128
P 7800 7350
F 0 "P5" H 7800 7550 50  0000 C CNN
F 1 "OUT1" V 7900 7350 50  0000 C CNN
F 2 "Connect:bornier3" H 7800 7350 50  0001 C CNN
F 3 "" H 7800 7350 50  0000 C CNN
	1    7800 7350
	1    0    0    1   
$EndComp
$Comp
L RELAY_SPDT K4
U 1 1 60D0C136
P 6850 7300
F 0 "K4" H 6800 7350 50  0000 C CNN
F 1 "FINDER-36.11.9.005" H 6850 6800 50  0000 C CNN
F 2 "myRelays:SRD-5VDC-SL-C" H 6850 7300 50  0001 C CNN
F 3 "" H 6850 7300 50  0000 C CNN
	1    6850 7300
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P6
U 1 1 60D0C7E2
P 7800 8500
F 0 "P6" H 7800 8700 50  0000 C CNN
F 1 "OUT1" V 7900 8500 50  0000 C CNN
F 2 "Connect:bornier3" H 7800 8500 50  0001 C CNN
F 3 "" H 7800 8500 50  0000 C CNN
	1    7800 8500
	1    0    0    1   
$EndComp
$Comp
L RELAY_SPDT K5
U 1 1 60D0C7F0
P 6850 8450
F 0 "K5" H 6800 8500 50  0000 C CNN
F 1 "FINDER-36.11.9.005" H 6850 7950 50  0000 C CNN
F 2 "myRelays:SRD-5VDC-SL-C" H 6850 8450 50  0001 C CNN
F 3 "" H 6850 8450 50  0000 C CNN
	1    6850 8450
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P7
U 1 1 60D0C7F8
P 7800 9650
F 0 "P7" H 7800 9850 50  0000 C CNN
F 1 "OUT1" V 7900 9650 50  0000 C CNN
F 2 "Connect:bornier3" H 7800 9650 50  0001 C CNN
F 3 "" H 7800 9650 50  0000 C CNN
	1    7800 9650
	1    0    0    1   
$EndComp
$Comp
L RELAY_SPDT K6
U 1 1 60D0C806
P 6850 9600
F 0 "K6" H 6800 9650 50  0000 C CNN
F 1 "FINDER-36.11.9.005" H 6850 9100 50  0000 C CNN
F 2 "myRelays:SRD-5VDC-SL-C" H 6850 9600 50  0001 C CNN
F 3 "" H 6850 9600 50  0000 C CNN
	1    6850 9600
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P8
U 1 1 60D0C80E
P 7800 10800
F 0 "P8" H 7800 11000 50  0000 C CNN
F 1 "OUT1" V 7900 10800 50  0000 C CNN
F 2 "Connect:bornier3" H 7800 10800 50  0001 C CNN
F 3 "" H 7800 10800 50  0000 C CNN
	1    7800 10800
	1    0    0    1   
$EndComp
$Comp
L RELAY_SPDT K7
U 1 1 60D0C81C
P 6850 10750
F 0 "K7" H 6800 10800 50  0000 C CNN
F 1 "FINDER-36.11.9.005" H 6850 10250 50  0000 C CNN
F 2 "myRelays:SRD-5VDC-SL-C" H 6850 10750 50  0001 C CNN
F 3 "" H 6850 10750 50  0000 C CNN
	1    6850 10750
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P9
U 1 1 60D0C824
P 7800 11950
F 0 "P9" H 7800 12150 50  0000 C CNN
F 1 "OUT1" V 7900 11950 50  0000 C CNN
F 2 "Connect:bornier3" H 7800 11950 50  0001 C CNN
F 3 "" H 7800 11950 50  0000 C CNN
	1    7800 11950
	1    0    0    1   
$EndComp
$Comp
L RELAY_SPDT K8
U 1 1 60D0C832
P 6850 11900
F 0 "K8" H 6800 11950 50  0000 C CNN
F 1 "FINDER-36.11.9.005" H 6850 11400 50  0000 C CNN
F 2 "myRelays:SRD-5VDC-SL-C" H 6850 11900 50  0001 C CNN
F 3 "" H 6850 11900 50  0000 C CNN
	1    6850 11900
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR013
U 1 1 60E1A1BB
P 3500 7700
F 0 "#PWR013" H 3500 7550 50  0001 C CNN
F 1 "VDD" H 3500 7850 50  0000 C CNN
F 2 "" H 3500 7700 50  0000 C CNN
F 3 "" H 3500 7700 50  0000 C CNN
	1    3500 7700
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR014
U 1 1 60E19F52
P 2650 7650
F 0 "#PWR014" H 2650 7500 50  0001 C CNN
F 1 "VDD" H 2650 7800 50  0000 C CNN
F 2 "" H 2650 7650 50  0000 C CNN
F 3 "" H 2650 7650 50  0000 C CNN
	1    2650 7650
	1    0    0    -1  
$EndComp
$Comp
L AddressSelector3 JP2
U 1 1 60E1B351
P 2400 7850
F 0 "JP2" H 2440 7940 50  0000 C CNN
F 1 "ADDRESS" H 2420 8010 50  0000 C CNN
F 2 "mySwitches:AdressSelector3" H 2400 7850 50  0001 C CNN
F 3 "" H 2400 7850 50  0000 C CNN
	1    2400 7850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 9700 2400 9700
Wire Wire Line
	2600 9300 2600 9700
Wire Wire Line
	2400 9900 3400 9900
Wire Wire Line
	2400 9800 3500 9800
Wire Wire Line
	2400 10000 3600 10000
Wire Wire Line
	2600 10000 2600 10100
Connection ~ 5850 6400
Connection ~ 5450 6400
Connection ~ 5450 8000
Wire Wire Line
	5450 8000 5850 8000
Wire Wire Line
	5050 7700 5150 7700
Wire Wire Line
	5450 7900 5450 8100
Wire Wire Line
	5050 7300 5550 7300
Wire Wire Line
	5850 8000 5850 7500
Wire Wire Line
	5850 6800 5850 7100
Wire Wire Line
	5850 6900 6150 6900
Connection ~ 5850 6900
Wire Wire Line
	5450 6300 5450 6500
Wire Wire Line
	5850 6400 5850 6500
Wire Wire Line
	5450 6800 5450 7500
Connection ~ 5450 7000
Connection ~ 5450 4100
Connection ~ 5450 5700
Wire Wire Line
	5450 5700 5850 5700
Wire Wire Line
	5050 5400 5150 5400
Wire Wire Line
	5450 5600 5450 5800
Wire Wire Line
	5050 5000 5550 5000
Wire Wire Line
	5850 5700 5850 5200
Wire Wire Line
	5850 4600 6150 4600
Wire Wire Line
	5850 4500 5850 4800
Connection ~ 5850 4600
Wire Wire Line
	5450 4000 5450 4200
Wire Wire Line
	5850 4100 5850 4200
Wire Wire Line
	5450 4500 5450 5200
Wire Wire Line
	5450 4700 6150 4700
Connection ~ 5450 4700
Wire Wire Line
	5450 4100 6450 4100
Wire Wire Line
	5450 6400 6450 6400
Wire Wire Line
	2600 9100 2600 9200
Wire Wire Line
	2600 9200 2400 9200
Wire Wire Line
	4150 8250 4250 8250
Wire Wire Line
	4250 8250 4250 5000
Wire Wire Line
	4350 5400 4350 8350
Wire Wire Line
	4350 8350 4150 8350
Wire Wire Line
	4150 8450 4450 8450
Wire Wire Line
	4450 8450 4450 7300
Wire Wire Line
	4450 7300 4750 7300
Wire Wire Line
	4750 7700 4550 7700
Wire Wire Line
	4550 7700 4550 8550
Wire Wire Line
	4550 8550 4150 8550
Wire Wire Line
	4150 8650 4550 8650
Wire Wire Line
	4550 8650 4550 9600
Wire Wire Line
	4550 9600 4750 9600
Wire Wire Line
	4150 8750 4450 8750
Wire Wire Line
	4450 8750 4450 10000
Wire Wire Line
	4150 8850 4350 8850
Wire Wire Line
	4350 8850 4350 11900
Wire Wire Line
	4150 8950 4250 8950
Wire Wire Line
	4250 8950 4250 12300
Wire Wire Line
	2900 9150 2900 9400
Wire Wire Line
	2400 9300 3100 9300
Connection ~ 2600 9300
Wire Wire Line
	3100 9300 3100 9400
Connection ~ 2900 9300
Wire Wire Line
	2900 9700 2900 9800
Connection ~ 2900 9800
Wire Wire Line
	3100 9700 3100 9900
Connection ~ 3100 9900
Wire Wire Line
	4350 5400 4750 5400
Wire Wire Line
	4250 5000 4750 5000
Wire Wire Line
	6150 4600 6150 4200
Wire Wire Line
	6150 4200 6450 4200
Wire Wire Line
	6150 4700 6150 5350
Connection ~ 5850 4100
Wire Wire Line
	6250 4100 6250 5250
Wire Wire Line
	6250 6400 6250 7550
Connection ~ 6250 4100
Connection ~ 6250 6400
Wire Wire Line
	6350 3900 6450 3900
Wire Wire Line
	6350 3600 6350 3900
Wire Wire Line
	6350 3600 7500 3600
Wire Wire Line
	7500 3600 7500 3900
Wire Wire Line
	7500 3900 7600 3900
Wire Wire Line
	7250 4000 7600 4000
Wire Wire Line
	7600 3800 7250 3800
Wire Wire Line
	6150 6900 6150 6500
Wire Wire Line
	5450 7000 6150 7000
Wire Wire Line
	6150 7000 6150 7650
Wire Wire Line
	4450 10000 4750 10000
Wire Wire Line
	4350 11900 4750 11900
Wire Wire Line
	4250 12300 4750 12300
Connection ~ 5850 11000
Connection ~ 5450 11000
Connection ~ 5450 12600
Wire Wire Line
	5450 12600 5850 12600
Wire Wire Line
	5050 12300 5150 12300
Wire Wire Line
	5450 12500 5450 12700
Wire Wire Line
	5050 11900 5550 11900
Wire Wire Line
	5850 12600 5850 12100
Wire Wire Line
	5850 11400 5850 11700
Wire Wire Line
	5850 11500 6150 11500
Connection ~ 5850 11500
Wire Wire Line
	5450 10900 5450 11100
Wire Wire Line
	5850 11000 5850 11100
Wire Wire Line
	5450 11400 5450 12100
Connection ~ 5450 11600
Connection ~ 5450 8700
Connection ~ 5450 10300
Wire Wire Line
	5450 10300 5850 10300
Wire Wire Line
	5050 10000 5150 10000
Wire Wire Line
	5450 10200 5450 10400
Wire Wire Line
	5050 9600 5550 9600
Wire Wire Line
	5850 10300 5850 9800
Wire Wire Line
	5850 9200 6150 9200
Wire Wire Line
	5850 9100 5850 9400
Connection ~ 5850 9200
Wire Wire Line
	5450 8600 5450 8800
Wire Wire Line
	5850 8700 5850 8800
Wire Wire Line
	5450 9100 5450 9800
Wire Wire Line
	5450 9300 6150 9300
Connection ~ 5450 9300
Wire Wire Line
	5450 8700 6450 8700
Wire Wire Line
	5450 11000 6450 11000
Wire Wire Line
	6150 9200 6150 8800
Wire Wire Line
	6150 9300 6150 9950
Connection ~ 5850 8700
Wire Wire Line
	6250 8700 6250 9850
Wire Wire Line
	6250 11000 6250 12150
Connection ~ 6250 8700
Connection ~ 6250 11000
Wire Wire Line
	6150 11500 6150 11100
Wire Wire Line
	5450 11600 6150 11600
Wire Wire Line
	6150 11600 6150 12250
Wire Wire Line
	6250 5250 6450 5250
Wire Wire Line
	6150 5350 6450 5350
Wire Wire Line
	6350 5050 6450 5050
Wire Wire Line
	6350 4750 6350 5050
Wire Wire Line
	6350 4750 7500 4750
Wire Wire Line
	7500 4750 7500 5050
Wire Wire Line
	7500 5050 7600 5050
Wire Wire Line
	7250 5150 7600 5150
Wire Wire Line
	7600 4950 7250 4950
Wire Wire Line
	6150 6500 6450 6500
Wire Wire Line
	6350 6200 6450 6200
Wire Wire Line
	6350 5900 6350 6200
Wire Wire Line
	6350 5900 7500 5900
Wire Wire Line
	7500 5900 7500 6200
Wire Wire Line
	7500 6200 7600 6200
Wire Wire Line
	7250 6300 7600 6300
Wire Wire Line
	7600 6100 7250 6100
Wire Wire Line
	6250 7550 6450 7550
Wire Wire Line
	6150 7650 6450 7650
Wire Wire Line
	6350 7350 6450 7350
Wire Wire Line
	6350 7050 6350 7350
Wire Wire Line
	6350 7050 7500 7050
Wire Wire Line
	7500 7050 7500 7350
Wire Wire Line
	7500 7350 7600 7350
Wire Wire Line
	7250 7450 7600 7450
Wire Wire Line
	7600 7250 7250 7250
Wire Wire Line
	6150 8800 6450 8800
Wire Wire Line
	6350 8500 6450 8500
Wire Wire Line
	6350 8200 6350 8500
Wire Wire Line
	6350 8200 7500 8200
Wire Wire Line
	7500 8200 7500 8500
Wire Wire Line
	7500 8500 7600 8500
Wire Wire Line
	7250 8600 7600 8600
Wire Wire Line
	7600 8400 7250 8400
Wire Wire Line
	6250 9850 6450 9850
Wire Wire Line
	6150 9950 6450 9950
Wire Wire Line
	6350 9650 6450 9650
Wire Wire Line
	6350 9350 6350 9650
Wire Wire Line
	6350 9350 7500 9350
Wire Wire Line
	7500 9350 7500 9650
Wire Wire Line
	7500 9650 7600 9650
Wire Wire Line
	7250 9750 7600 9750
Wire Wire Line
	7600 9550 7250 9550
Wire Wire Line
	6150 11100 6450 11100
Wire Wire Line
	6350 10800 6450 10800
Wire Wire Line
	6350 10500 6350 10800
Wire Wire Line
	6350 10500 7500 10500
Wire Wire Line
	7500 10500 7500 10800
Wire Wire Line
	7500 10800 7600 10800
Wire Wire Line
	7250 10900 7600 10900
Wire Wire Line
	7600 10700 7250 10700
Wire Wire Line
	6250 12150 6450 12150
Wire Wire Line
	6150 12250 6450 12250
Wire Wire Line
	6350 11950 6450 11950
Wire Wire Line
	6350 11650 6350 11950
Wire Wire Line
	6350 11650 7500 11650
Wire Wire Line
	7500 11650 7500 11950
Wire Wire Line
	7500 11950 7600 11950
Wire Wire Line
	7250 12050 7600 12050
Wire Wire Line
	7600 11850 7250 11850
Wire Wire Line
	2650 7650 2650 8150
Wire Wire Line
	2650 7850 2600 7850
Wire Wire Line
	2650 8000 2600 8000
Connection ~ 2650 7850
Wire Wire Line
	2650 8150 2600 8150
Connection ~ 2650 8000
Wire Wire Line
	2300 7850 2200 7850
Wire Wire Line
	2200 7850 2200 8250
Wire Wire Line
	2300 8150 2200 8150
Connection ~ 2200 8150
Wire Wire Line
	2300 8000 2200 8000
Connection ~ 2200 8000
Wire Wire Line
	3500 7700 3500 7800
Wire Wire Line
	2750 8200 2450 8200
Wire Wire Line
	2450 7900 2850 7900
Wire Wire Line
	3500 9800 3500 9400
Wire Wire Line
	3400 9900 3400 9400
Wire Wire Line
	2750 8400 2750 8200
$Comp
L VSS #PWR015
U 1 1 60E5C8B3
P 3600 9900
F 0 "#PWR015" H 3600 9750 50  0001 C CNN
F 1 "VSS" H 3600 10050 50  0000 C CNN
F 2 "" H 3600 9900 50  0000 C CNN
F 3 "" H 3600 9900 50  0000 C CNN
	1    3600 9900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 10000 3600 9900
Connection ~ 2600 10000
$Comp
L Conn_01x03 J1
U 1 1 6113EDAB
P 2200 9300
F 0 "J1" H 2200 9500 50  0000 C CNN
F 1 "PWR" V 2300 9300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 2200 9300 50  0001 C CNN
F 3 "" H 2200 9300 50  0001 C CNN
	1    2200 9300
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x04 J2
U 1 1 6113F0CE
P 2200 9900
F 0 "J2" H 2200 10100 50  0000 C CNN
F 1 "IN" V 2300 9850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 2200 9900 50  0001 C CNN
F 3 "" H 2200 9900 50  0001 C CNN
	1    2200 9900
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR016
U 1 1 6113F675
P 2500 9500
F 0 "#PWR016" H 2500 9250 50  0001 C CNN
F 1 "GND" H 2500 9350 50  0000 C CNN
F 2 "" H 2500 9500 50  0000 C CNN
F 3 "" H 2500 9500 50  0000 C CNN
	1    2500 9500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 9400 2500 9400
Text Notes 2100 8850 0    60   ~ 0
Jumper on J1 2&3\nfor 5V power\nElse power on 1&3
Wire Wire Line
	2750 8400 3050 8400
Wire Wire Line
	3050 8300 2800 8300
Wire Wire Line
	2800 8300 2800 8050
Wire Wire Line
	2800 8050 2450 8050
Wire Wire Line
	2850 7900 2850 8200
Wire Wire Line
	2850 8200 3050 8200
Wire Wire Line
	2500 9400 2500 9500
$EndSCHEMATC
