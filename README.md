# RELAY MODULES

The purpose of this page is to explain the realization of some relay module, including bistable models and I2C modules.

The boards use the following components :

 * 1 or more relays
 * some PN2222 transistors
 * some resistances
 * some diodes
 * some connectors
 * 1 PCF8574 expander
 * 1 MCP23008 expander
 * 1 ULN2803

### ELECTRONICS

The schemas are made using KICAD.

### BLOG
A description in french here : 

