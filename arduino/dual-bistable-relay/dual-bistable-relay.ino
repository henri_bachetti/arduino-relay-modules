
// 2 latch relays with L293D

// digital pins
#define ENABLE    2
#define RESET1    3
#define SET1      4
#define RESET2    5
#define SET2      6


void setup()
{
  Serial.begin(115200);
  pinMode(ENABLE, OUTPUT);
  pinMode(RESET1, OUTPUT);
  pinMode(SET1, OUTPUT);
  pinMode(RESET2, OUTPUT);
  pinMode(SET2, OUTPUT);
}

void relay1_On(void)
{
  Serial.println("relay 1 ON");
  digitalWrite(ENABLE, HIGH);
  digitalWrite(SET1, HIGH);
  delay(100);
  digitalWrite(SET1, LOW);
  digitalWrite(ENABLE, LOW);
}

void relay1_Off(void)
{
  Serial.println("relay 1 OFF");
  digitalWrite(ENABLE, HIGH);
  digitalWrite(RESET1, HIGH);
  delay(100);
  digitalWrite(RESET1, LOW);
  digitalWrite(ENABLE, LOW);
}

void relay2_On(void)
{
  Serial.println("relay 2 ON");
  digitalWrite(ENABLE, HIGH);
  digitalWrite(SET2, HIGH);
  delay(100);
  digitalWrite(SET2, LOW);
  digitalWrite(ENABLE, LOW);
}

void relay2_Off(void)
{
  Serial.println("relay 2 OFF");
  digitalWrite(ENABLE, HIGH);
  digitalWrite(RESET2, HIGH);
  delay(100);
  digitalWrite(RESET2, LOW);
  digitalWrite(ENABLE, LOW);
}

void loop()
{
  relay1_On();
  delay(1000);
  relay1_Off();
  delay(1000);
  relay2_On();
  delay(1000);
  relay2_Off();
  delay(1000);
}
